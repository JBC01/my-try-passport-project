var express = require('express');
var router = express.Router();
var passport = require('passport')
	, LocalStrategy = require('passport-local').Strategy;

var bcrypt = require('bcrypt');

Object.assign = require('object-assign');

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const saltRounds = 11;
const salt = bcrypt.genSaltSync(saltRounds);

var Models = require('../database/models');

router.use(function (req, res, next) {
	res.setHeader('Content-Type', 'application/json');
	next();
});

// Display all users

router.get('/users', function(req, res, next){
	Models.User.find({}).exec(function (err, data) {
		if (!err) {
			res.send(JSON.stringify(data));
			res.status(200);
		} else {
			res.send(JSON.stringify({
				message: "Unable to get data"
			}));
			res.status(500);
		}
	});
});

// Add a user

router.post('/user/new', function(req, res, next){
	if (req.body && req.body.name && req.body.username && req.body.password && req.body.email && req.body.email.match(emailRegex)) {
		let User = new Models.User({
			name: req.body.name,
			username: req.body.username,
			email: req.body.email,
			password: bcrypt.hashSync(req.body.password, salt)
		}).save(function (error, data) {
			if (!error) {
				res.send(JSON.stringify(data));
				res.status(201);
			} else {
				res.send(JSON.stringify({
					message: "Internal Server Error"
				}));
				res.status(500);
			}
		});
	} else {
		res.send(JSON.stringify({
			message: "Missing information"
		}));
		res.status(400);
	}
});


// authentication
// TODO : redirect
router.post('/login', function(req, res, next) {
	passport.authenticate('local', function (err, user, info) {
		if (err) {
			return next(err);
		}
		if (!user) {
			return res.redirect('/login');
		}
		req.logIn(user, function (err) {
			if (err) {
				return next(err);
			}
			return res.redirect('/test')
		});
	})(req, res, next);
});

router.post('/logout', function (req, res, next) {

});


router.use(function(req, res, next){
	res.send(JSON.stringify({
		error: 404
	}));
});

module.exports = router;
