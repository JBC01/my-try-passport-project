var express = require('express');
var router = express.Router();

var Models = require('../database/models');

router.use(function(req, res, next) {
	res.render('index', {});
});

module.exports = router;
