let mongoose = require('../');

let UserSchema = mongoose.Schema({
	name: {
		type: String,
		index: true
	},
	username: {
		type: String,
		index: true
	},
	email: {
		type: String,
		index: true
	},
	password: {
		type: String
	}
});

module.exports = mongoose.model('User', UserSchema);