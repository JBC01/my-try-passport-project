var path = require('path');
var webpack = require('webpack');

module.exports = {
	entry: './public/javascripts/src/main.js',
	output: {
		path: path.resolve(__dirname, './public/javascripts/'),
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				loader: "babel-loader",
				exclude: /node_modules/,
				query: {
					presets: ['es2016', 'react'],
					plugins: [
						"babel-plugin-transform-object-rest-spread"
					].map(require.resolve)
				}
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			}
		]
	},
	node: {
		fs: "empty"
	}
};
