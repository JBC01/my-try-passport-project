import React from 'react';
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import {Router, Route, browserHistory} from 'react-router';
import {syncHistoryWithStore, routerReducer} from 'react-router-redux';

import * as reducers from './reducers';

const store = createStore(
	combineReducers({
		...reducers,
		routing: routerReducer
	}),
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const history = syncHistoryWithStore(browserHistory, store);

import Layout from './components/layout.jsx';
import Login from './components/login.jsx'
import Test from './components/test.jsx'

export default class App extends React.Component {
    render(){
        return <Provider store={store}>
            <Router history={history}>
                <Route path="" component={Layout}>
					<Route path="login" component={Login}/>
					<Route path="test" component={Test} />
                </Route>
            </Router>
        </Provider>
    }
}
