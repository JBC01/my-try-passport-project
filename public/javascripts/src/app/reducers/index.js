import {LOCATION_CHANGE} from 'react-router-redux'

export const ui = function(state, action){
    switch(action.type){
        case "SHOW_NAV":
            return Object.assign({}, state, {
                nav_hidden: false
            });
            break;
        case "HIDE_NAV":
            return Object.assign({}, state, {
                nav_hidden: true
            });
            break;
        case "LOAD_SUBJECT":
            return Object.assign({}, state, {
                breadcrumb: Object.assign({}, state.breadcrumb, {
                    subject: action.payload.name
                })
            });
            break;
        case "LOAD_MARK":
            return Object.assign({}, state, {
                breadcrumb: Object.assign({}, state.breadcrumb, {
                    mark: action.payload
                })
            });
            break;
        case "LOAD_CHAPTER":
            return Object.assign({}, state, {
                breadcrumb: Object.assign({}, state.breadcrumb, {
                    chapter: action.payload
                })
            });
            break;
        case LOCATION_CHANGE:
            if(action && action.payload) switch (action.payload.pathname) {
                case "/":
                    return Object.assign({}, state, {
                        breadcrumb: {
                            subject: false,
                            mark: false,
                            chapter: false,
                            part: false
                        }
                    });
                    break;
            }
    }
    return Object.assign({
        nav_hidden: false,
        breadcrumb: {
            subject: [],
            mark: false,
            chapter: false,
            part: false
        }
    }, state)
};

export const data = function(state, action){
    switch(action.type){
        case "LOAD_SUBJECTS":
            return Object.assign({}, state, {
                subjects: action.payload
            });
            break;
        case "LOAD_CHAPTERS":
            return Object.assign({}, state, {
                chapters: action.payload
            });
            break;
        case "LOAD_SUBJECT":
            return Object.assign({}, state, {
                subject: action.payload
            });
            break;
        case "LOAD_MARKS":
            return Object.assign({}, state, {
                marks: action.payload
            });
            break;
        case "LOAD_PERIODS":
            return Object.assign({}, state, {
                periods: action.payload
            });
            break;
        case "LOAD_ENGLISH_WORDS":
            return Object.assign({}, state, {
                englishWords: action.payload
            });
            break;
        case "LOAD_MARK":
            return Object.assign({}, state, {
                mark: action.payload
            });
            break;
		case "LOAD_CHAPTER":
			return Object.assign({}, state, {
				chapter: action.payload
			});
			break;
        default:
            if(action && action.payload) switch (action.payload.pathname) {
                case "/":
                    return Object.assign({}, state, {
                        subject: [],
                        chapter: false,
                        mark: false
                    });
                    break;
            }
            return Object.assign({
                subject: [],
                mark: false,
				chapter: [],
                subjects: [],
                periods: [],
                englishWords: [],
                chapters: [],
                marks: []
            }, state);
            break;
    }
};
