import React from 'react';
import superagent from 'superagent';

export default class Login extends React.Component {
	constructor(props){
		super(props);
		this.state = {};
		this.save = this.save.bind(this);
	}
	save(e){
		e.preventDefault();
		if (this.refs.user.value && this.refs.pass.value) {
			superagent.post('/api/login').send({
				username: this.refs.user.value,
				password: this.refs.pass.value
			}).end(function(err, res){
				if (err) {
					console.log(err);
				}
			});
		} else {
			alert("An information is void");
		}
	}
	render(){
		return <div className="logWrapper">
			<h2>Enter your credentials</h2>
			<form onSubmit={this.save}>
				<input type="text" placeholder="Username" ref="user"/><br/><br/>
				<input type="text" placeholder="Password" ref="pass"/><br/><br/>
				<input type="submit" value="Submit"/>
			</form>
		</div>
	}
}